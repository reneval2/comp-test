import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { NoContentComponent } from './no-content';
import { ColorsComponent } from './colors';

export const ROUTES: Routes = [
  {path : '', component : HomeComponent},
  {path : 'home', component : HomeComponent},
  {path : 'colors', component : ColorsComponent},
  {path : '**', component : NoContentComponent},
];

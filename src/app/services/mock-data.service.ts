import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MockDataService {
  constructor(private _http: Http) {
  }

  public getData() {
    return this._http.get('/assets/mock-data/mock-data.json')
      .map((res) => {
        return res.json()['colorsArray'];
      });
  }
}

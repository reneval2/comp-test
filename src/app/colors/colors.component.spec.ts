import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';
import 'rxjs/observable/of';
import { Observable } from 'rxjs';

// Load the implementations that should be tested
import { ColorsComponent } from './colors.component';
import { MockDataService } from '../services/mock-data.service';

class MockDataServiceForTest {
  public getData() {
    return Observable.of(
      [{colorName : 'red', hexValue : '#f00'},
        {colorName : 'black', hexValue : '#000'}]);
  }
}

describe('Colors page component', () => {
  beforeEach(() => {
    // jasmine.addMatchers(customMatcher);
    TestBed.configureTestingModule({
      providers : [
        {
          provide : ActivatedRoute,
          useValue : {
            data : {
              subscribe : (fn: (value: Data) => void) => fn({
                yourData : 'yolo'
              })
            }
          }
        },
        {
          provide : MockDataService,
          useClass : MockDataServiceForTest
        },
        ColorsComponent
      ]
    });

  });

  it('should contain red color', inject([ColorsComponent], (colorsComponent: ColorsComponent) => {
    colorsComponent.ngOnInit();
    expect(colorsComponent.colors)
      .toContain(jasmine.objectContaining({colorName : 'red'}));
  }));

});

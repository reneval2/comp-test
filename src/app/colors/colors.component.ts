import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MockDataService } from '../services/mock-data.service';

@Component({
  selector : 'about',
  styles : [
    require('./colors.less').toString()
  ],
  templateUrl : 'colors.component.html'
})
export class ColorsComponent implements OnInit {

  public colors: any;

  constructor(private  _mockDataService: MockDataService,
              public route: ActivatedRoute) {
  }

  public ngOnInit() {
    this._mockDataService
      .getData()
      .subscribe((data: any) => {
        this.colors = data;
        console.log(data);
      });
  }

  public  invertColor(hexTripletColor) {
    let color = hexTripletColor;
    color = color.substring(1);
    color = parseInt(color, 16);
    color = 0xFFFFFF ^ color;
    color = ('000000' + color).slice(-6);
    color = '"#' + color;
    return color;
  }
}
